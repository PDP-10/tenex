^^
.title	Notes on MACN11 Version 4
.left margin 5
.right margin 70
.tab stops 5 10 15 20 25 30 35
.spacing 1
.paragraph


MACN11 is a PDP-11 MACRO language assembler which operates
on the PDP-10.  This document describes version 4 of MACN11;
with the exception of differences listed in the
following pages, it is compatible with the language accepted by
MACRO-11 under DOS Version 9, as described in DEC's DOS/BATCH assembler manual
(order no. DEC-11-LASMA-A-D).
.paragraph


Bug reports, comments, and suggestions concerning either
version of MACN11 are welcome via net mail to Raveling@ISIB.
.subtitle	MACRO-11 Language Discrepancies
.page
.skip 3

Features not yet supported
.break
--------------------------
.skip 2

 .DSABL	REG	-- Parsed properly but ignored;
.break
		Register definitions are always enabled
.break
		at present.
.skip 1
 .MCALL		-- Ignored;  At present system macro definitions
.break
		invoked via .MCALL must be supplied by specifying
.break
		SYSMAC.SML as the first input file.
.skip 2

	Both of these features will be supported in a future version.

.skip 5
Sundry differences
.break
------------------
.skip 2

Default modes for the .LIST/.NLIST and .ENABL/.DSABL directives
can be changed by assembly parameters in MACN11.  In the configuration
maintained by ISI these defaults match MACRO-11's except for
the following:
.skip 1
	.NLIST	TTM	; [format listings for lpt output]
.break
	.ENABL	LC	; [don't fold lower case]
.skip 2
The following special characters are legal in MACN11 source code:
.skip 1
	escape, _^N, _^O, `, {, }, |, ~


.subtitle	Language Extensions
.page
Language extensions
.break
-------------------
.skip 1
.indent -5

 .ASCII _& .ASCIZ  
.skip 1
MACN11 allows multiple expressions, rather than just one,
within brackets.  Expressions are delimited by commas.
E.g., "<cr><lf>" can be written as "<cr,lf>".
.skip 2
.indent -5
 .ENABL _& .DSABL
.skip 1
These verbs recognize three nonstandard operands:
.skip 1
.indent -3
NSF  (Nonstandard features):  Enabled by default.
.break
Enabling NSF allows use of MACN11's extended features.
Disabling limits the acceptable source language to
being strictly compatible with MACRO-11.
.skip 1
.indent -3
ISD   (Internal Symbol Definitions):  Disabled by default
.break
Enabling ISD includes definitions of internal symbols, as
well as global symbols, in the object module output.  These 
definitions allow generation of a symbol table for debuggers
such as DDT-11.
.skip 1
.indent -3

HOV  (Hexadecimal override):  Disabled by default.
If the current radix is 16 and HOV is enabled,
terms of expressions which begin with A-F are parsed as
hex numbers, rather than symbols.   E.g.,
.nofill
.skip 1
 .ENABL	HOV		; Enabling hex override causes
.break
 MOV 	_#A,R0	; this instruction to load 10. into
.break
				; R0, rather than the address of a
.break
				; word labelled A.
.fill
.skip 1
.left margin 5
.indent -5
 .ENDM 
.skip 1

Interior macro definitions within a nest can be terminated
by a .ENDM whose operand is the name of an exterior definition.
.skip 2
.indent -5
Hexadecimal numbers
.skip 1

Hex numbers, using the digits 0-9 and A-F, may be used
via extensions of the existing radix control functions.
 .RADIX accepts 16 as a valid operand to set the current
default radix, and temporary radix control includes "_^H"
in addition to the standard "_^D", "_^O", and "_^B".
.skip 1
Using hex as the default radix may require preceding some
hex numbers with a 0 digit.  For example, "0BAD" is clearly
a hex number, but "BAD" is normally assumed to be a symbol.
This assumption can be changed by ".ENABL HOV".
.page
.indent -5
 .IF variants
.skip 1
.indent -3
ABS -- ".IF ABS <address-expression>" causes assembly of the conditional
block which it heads if the address expression given is absolute, rather
than relocatable.  Absolute expressions are those which can be evaluated
entirely by the assembler; they must contain no global references and no
references to labels within relocatable program sections (control sections)
other than those of the form "a-b", where a and b are in the same section.
.skip 1
.indent -3
EQV -- ".IF EQV <address-expression>,<address-expression>" assembles
the conditional block which it heads if both expressions are equivalent.
They are defined as equivalent when they have the same type, addressing
mode, and value.  For example, ZERO is equivalent to 0 if the former
is defined by "ZERO = 0", but neither is equivalent to R0, %0, or GZERO,
where GZERO is a global symbol whose value may be 0.
.skip 1
.indent -3
NQV  --  Identical to .EQV except that it assembles the subsequent
conditional block if the two expressions are not equivalent.
.skip 1
.indent -3
**** Warning:  Address expressions in these .IF variants should not
refer to symbols which are defined at a later point in the source input.
An attempt to do this normally causes phase errors (P flags) on every
symbol definition following the .IF.
.skip 2
.indent -5
 .LIST _& .NLIST
.skip 1
Two additional listing control operands are recognized:
.skip 1
.indent -3
ASC  (ASCII binary expansion):  Default is .NLIST ASC.
.break
Specifying ".NLIST ASC" suppresses listing of the binary code generated
by .ASCII and .ASCIZ directives.  This is effectively a selective
".NLIST BIN" for the sake of listing readability where the normal
alternative of ".NLIST BEX" may be undesirable.
.skip 1
.indent -3
SON  (Source Oriented Numbering):  Default is .NLIST SON
.break
 .LIST SON in the source, or /LI:SON in the command dialog, specifies
source-oriented numbering of listing pages.  In this format page numers
appear as "n-m", where n is incremented only when a form feed is read
from the source input; m is a page number extension which identifies
output pages following the most recent form feed.  The default for
this option is .NLIST SON, for sequential numbering of output pages
regardless of input file format.
.page
.indent -5
 .INSERT
.skip 1
".INSERT###filename" logically inserts the named file into the source
by reading it immediately.  This is convenient for specifying macro
libraries required for an assembly, eliminating the need for users to
type these file names in their command dialog.
.skip 2
.indent -5
Logical shift operator ("__")
.skip 1
An additional binary operator is recognized in expression evaluation.
"a__b" is evaluated as "the value of a shifted left b bits".  If b
is negative, the term is shifted right.  Both a and b may be any
valid term, including a bracketed expression.
.skip 1
A common use for this operator is in defining symbolic values for
bit masks.  For example, "INTENA = 1__6"  defines an interrupt
enable mask as a 1 in bit 6.
.indent -5
 .NKIND
.skip 1
 .NKIND is analagous to .NTYPE, but instead of returning an addressing
mode it returns flag bits to indicate the type of the argument.
".NKIND  symbol,<address-expression>" sets symbol to the expression's
type bits, which are defined as follows:
.skip 1
.indent 5
1  (bit 0):  1 if relocatable, 0 if absolute
.break
.indent 5
2  (bit 1):  1 if global
.break
.indent 5
4  (bit 2):  1 if expression defines a register
.break
.indent 5
8  (bit 3):  1 if the expression is a label on a line
.break
.indent 18
of source code
.break
.indent 5
16 (bit 4):  1 if defined, 0 if undefined
.skip 2
.indent -5
 .PDP10
.skip 1
The ".PDP10" directive is recognized, but is ignored.
This is for the sake of assembling Bliss-11 compiler output.
.skip 1
.skip 2
.indent -5
Predefined symbols
.skip 1

Symbols can be predefined by assembling their symbol
table entries into MACN11.  At present register names
(R0-R7, SP, and PC) and ".MACN." are defined in this way.
 .MACN. is equated to the assembler's version number.
.if -1
 .VRSN. is set to the Tenex version number of the source file
currently being read; if the source is not on disk .VRSN. is undefined.
.fi
.page
.indent -5

Radix 50 terms in expressions:  _^R
.skip 1
"_^R" is recognized as a unary operator in expressions,
indicating that the next three characters are to be
converted to radix 50 format for use as a 16-bit value.
Any character without a radix 50 equivalent is treated
as a delimiter, and is taken as equivalent to trailing
blanks in evaluating the term.
.skip 2
.indent -5
 .REQUIRE
.skip 1
 ".REQUIRE###filename" performs exactly the same function as
 ".INSERT###filename", except that it also forces a page skip in the
assembly listing before the file is read.
.skip 2
.indent -5

 .SBHED 
.skip 1
A new directive, .SBHED, specifies a subheading;  it functions
as a conditional .PAGE followed by a .SBTTL.  It skips to a new
page unless the most recent listing output was a page skip,
then outputs a standard page heading.  .SBHED's operand
field is included in the table of contents and in all page
subheadings until another .SBHED or a .SBTTL is issued.
The .SBHED directive itself is the first line listed below
the new page heading.

.subtitle	Operating Procedures
.page
.left margin 0
Operating procedures
.break
--------------------
.paragraph
Operating procedures for MACN11  are nearly
identical to MACRO-11's.  The only differences are the following:
.skip 1
.left margin 8
.indent -3
1.#"=" and "__" are accepted as synonyms for "<".
.skip 1
.indent -3
2.#The /PA switch is accepted but is ignored.
.skip 1
.indent -3
3.#An additional switch, /FO, is available to specify
the PDP-10 file format for object output.
.skip 1
.indent -3
4.#Cross reference tables are available only by invoking
CREF manually to process MACN11's listing output file.
.skip 1
.indent -3
5.#Another added switch, "/EQ", sets specified assembly parameters
to zero.  For example, "/EQ:A:B" is equivalent to supplying
"A=0" and "B=0" in the source.
.if -1
.skip 1
.indent -3
6.#The command dialog accepts full Tenex file names,
including directory names and file version numbers,
and performs file name recognition as usual with escape
and control-F.
.fi
.left margin 0
.paragraph
Details of these differences are included in the description below.
.skip 3
.paragraph

MACN11 assembles one or more source files, normally producing
two files:  a relocatable object module and an assembly listing.
Command string syntax is similar to MACRO-11's.
.paragraph
Object files normally are generated in packed format, with each
PDP-11 word right-adjusted in a PDP-10 halfword.  This format is
standard for other utilities which may deal with these files
(LNKX11, LINK11, FILEX, etc.).
.page
Command Input String
.break
--------------------
.skip 1
.paragraph
When MACN11 is ready for a command it lists its name and
version number on the user's terminal, then outputs a "*" on
a new line.  In return it expects the following sort of string:
.skip 2

object,listing<source1,source2,...,sourcen
.skip 2
.left margin 5
.indent -5
"<" separates output file specifications from input specs.  "="
and "__" are equally acceptable.
.skip 1

.if -1
Each field in the command string specifies a file name and
may include optional switches.  File names are in standard
Tenex format  -- "<directory>filename.extension;version" --
with the usual defaulting conventions for directory and version.
If the extension is omitted MACN11 chooses a default appropriate
to the type of file (object, listing, or source).
.fi
.if 1
Each field in the command string specifies a file name and may
include optional switches.  File names must conform to
DEC conventions (6-byte file name, optional 3-byte
extension).  If the extension is omitted on any file name
the assembler chooses a suitable default.
.skip 1
A PPN (project-programmer number) may be supplied where MACRO-11
would accept a UIC (user identification code).  Under Tenex
this is treated as a directory number.  MACN11's source contains
assembly parameters to allow it to accept PPNs in the formats
used by CMU and Stanford.
.fi
.skip 2
.indent -5
"Object" is normally a relocatable object module file, ready for
input to LNKX11 or LINK-11.  Its default extension is
".OBJ".   Absolute binary output, with default extension
".BIN", can be generated by using ".ENABL ABS" in the
source program, or by including a "/EN:ABS" switch
in the command string.
.skip 1
Object file format may be specified with the "/FO" switch.
Switch values set three independent paramters, with an effective
default of "/FO:P:NAL:NOP".  The following values are legal:
.skip 1
Basic format selection:
.skip 1
.indent 5
P	Packed format -- one PDP-11 word right-adjusted in
.break
.indent 5
	each PDP-10 half word
.break
	.indent 5
I	Image format -- one PDP-11 byte right-adjusted in each
.break
.indent 5
	PDP-10 word
.skip 1
Alignment of the start of output blocks in PDP-10 words for packed
format output:
.skip 1
.indent 5
NAL	No alignment
.break
.indent 5
HAL	Half word alignment
.break
.indent 5
WAL	Word alignment
.skip 1
Padding between blocks:
.skip 1
.indent 5
NOP	No padding
.break
.indent 5
PAD	Standard padding -- 8 bytes for .OBJ files,
.break
.indent 5
	6 bytes for .BIN files
.skip 3
.indent -5
"Listing" is an ASCII file with a default extension of ".LST".
Cross references may be obtained by using a "/CRF" switch
with the listing file; this generates a file with extension ".CRF",
which must subsequently be processed by CREF.
.skip 3
.indent -5
Source files are in ASCII, with the following default extensions
recognized by the assembler:
.skip 1

	".M11", ".P11", ".MAC", blank
.skip 1

.left margin 5
.paragraph
Either the object file or the listing file, but not both, may be
omitted.  Any number of source files (except 0) may be used.
.if -1
.skip 3
While an assembly is running it's progress may be monitored by typing
control-Q.  MACN11 responds to this identifying the assembler pass
(1 or 2), the source file being processed, and the current input
line number.
.fi
